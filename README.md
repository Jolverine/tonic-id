## Tonic identification code

USAGE: ```./gettonic <wavfile> <metadata> <sec> <percent> <algo>```

```
gettonic
Inputs:
filename - Name of wav file
metadata - 1 male; 2 female; 3 Instrumental; 4 - not known; Default = 4
sec - No.of seconds of sampling to be taken
per - Percentage of low energy frames to be taken
algo - 1 for enmf, 2 for ceppitch
```

## Compilation

```gcc getTonicDrone.c -o gettonic -lm -lsndfile```

## Papers

* https://www.iitm.ac.in/donlab/website_files/thesis/MS/AUTOMATIC_TONIC_IDENTIFICATION_IN_INDIAN_CLASSICAL_MUSIC.pdf
* https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6811745
* https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6487983
